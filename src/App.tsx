import * as React from 'react';
import './assets/css/main.css';
import Header from './containers/Header';
import Main from './components/Main';
import Player from './containers/Player';
import { Provider } from 'react-redux';
import { store } from './state/store';

class App extends React.Component {
  public render() {
    return (
      <Provider store={store}>
        <div className="App">
          <Header />
          <Main />
          <Player />
        </div>
      </Provider>
    );
  }
}

export default App;
