#ngrok integration

Probably you haven't a public IP or a server to deploy your application. If you aim to use your own computer as a server despite of being behind a firewall or a private network, you can easily enable a tunnel using ngrok. Create an account in http://ngrok.com or access with your GitHub profile. Follow this 5-step installation process:

1. Download the ngrok binary according to your operating system.
2. Unzip the downloaded file and copy its content in the project folder.
3. Add the auth token (it is a unique id that you can find in your ngrok dashboard) as follows:
`./ngrok auth {token}`
This commands will store the auth token locally in a file named ngrok.yml (the exact folder depends on the operating system and user), so it will be unique for all the projects that you'd develop.
4. Launch the application either on development or production mode. The server will be launched in a different port. By default, development server is launched in port 3000 and production server in port 5000.
5. Launch ngrok indicating the port as follows (3000 for example):
`./ngrok http 3000`
